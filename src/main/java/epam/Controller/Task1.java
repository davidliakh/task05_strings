package epam.Controller;

import epam.Model.FileReader;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task1 {
    public static void main(String[] args) {
        FileReader f1 = new FileReader();
        int count = 0;
        String arr = f1.reader().toString();
        Pattern p = Pattern.compile("[^.+$\\n]");
        Matcher m = p.matcher(arr);
        while (m.find()) {
           System.out.print(m.group());
            count++;
        }
        System.out.println("\n"+count);
    }
}
