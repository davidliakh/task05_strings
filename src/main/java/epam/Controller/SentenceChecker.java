package epam.Controller;

import epam.Model.PrintableInterface;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class SentenceChecker implements PrintableInterface {

    private static String regex = "\\w+.*\\.";

    public SentenceChecker() {
    }

    public static boolean checkIsSentence(final String text) {
        Pattern p = Pattern.compile(regex);
        Matcher matcher = p.matcher(text);
        return matcher.matches(); //True if matches
    }

    @Override
    public void show() {
        String text1 = "hello, friend."; //No capital
        String text2 = "Hello, my dear friend"; //No dot ending
        String text3 = "This story is about magic."; //Correct
        System.out
                .println("Original: " + text1 + "\nResult: " + SentenceChecker.checkIsSentence(text1));
        System.out
                .println("Original: " + text2 + "\nResult: " + SentenceChecker.checkIsSentence(text2));
        System.out
                .println("Original: " + text3 + "\nResult: " + SentenceChecker.checkIsSentence(text3));
    }
}
