package epam.Controller;

import epam.Model.PrintableInterface;

import java.util.Arrays;

public class SplitReplaceTask implements PrintableInterface {

    private static void showSplit() {
        String text = "Hello, world! So, you think the best icecream is chocolate flavor one?";
        String[] words = text.split("(the)+|(you)+");
        System.out.println("Original text:\n" + text);
        System.out.println("Splited by \"the\" or \"you\"");
        Arrays.stream(words).forEach(System.out::println);
    }

    private static void showReplaceVowels() {
        String vowelRegex = "[aouie]+";
        String text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
                + "\nPhasellus vestibulum iaculis interdum."
                + "\nVestibulum ante ipsum primis in faucibus orci luctus"
                + " et ultrices posuere cubilia Curae;"
                + "\nMaecenas gravida, diam quis mollis vehicula,"
                + "\njusto massa cursus risus, tristique volutpat tellus metus sed diam";
        System.out.println("Original:\n" + text);
        System.out.println("After replacement:\n" + text.replaceAll(vowelRegex, "_"));
    }

    @Override
    public void show() {
        showSplit();
        showReplaceVowels();
    }
}