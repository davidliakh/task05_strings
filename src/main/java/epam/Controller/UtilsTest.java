package epam.Controller;

import epam.Model.PrintableInterface;
import epam.Model.StringUtils;

public class UtilsTest implements PrintableInterface {
    @Override
    public void show() {
        String name = "Davyd";
        Integer num = 20;
        Double num2 = 1999.0;
        StringUtils<Object> stringUtils = new StringUtils<Object>(name,num,num2);
        System.out.println(stringUtils.getString());
    }
}
