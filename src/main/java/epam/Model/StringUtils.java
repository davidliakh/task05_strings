package epam.Model;

public class StringUtils <T> {
    private String string;

    public StringUtils(T... objects) {
        string = "";
        for (T obj : objects) {
            string += (obj.toString() + " ");
        }
    }

    public String getString() {
        return string;
    }
}