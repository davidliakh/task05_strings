package epam.View;

import epam.Controller.SentenceChecker;
import epam.Controller.SplitReplaceTask;
import epam.Controller.UtilsTest;
import epam.Model.PrintableInterface;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Scanner;

public class Menu {

    private Map<String, String> menu;
    private Map<String, PrintableInterface> methodsMenu;
    private ResourceBundle bundle;
    private Locale locale;
    private Scanner input = new Scanner(System.in);

    public Menu() {
        UtilsTest utilsTest = new UtilsTest();
        SentenceChecker sentenceChecker = new SentenceChecker();
        SplitReplaceTask splitReplaceTask = new SplitReplaceTask();
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", utilsTest::show);
        methodsMenu.put("2", sentenceChecker::show);
        methodsMenu.put("3", splitReplaceTask::show);
        methodsMenu.put("4", this::internationalizeMenuEnglish);
        methodsMenu.put("5", this::internationalizeMenuUkrainian);
        methodsMenu.put("6", this::internationalizeMenuSpanish);
    }

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2")); //SentenceChecker
        menu.put("3", bundle.getString("3")); //Split and Replace
        menu.put("4", bundle.getString("4")); //Change to en
        menu.put("5", bundle.getString("5")); //Change to uk
        menu.put("6", bundle.getString("6")); //Change to es
        menu.put("Q", bundle.getString("Q")); //Quit
    }

    private void internationalizeMenuEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuUkrainian() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuSpanish() {
        locale = new Locale("es");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet()) {
            if (key.length() == 1) {
                System.out.println(menu.get(key));
            }
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).show();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
